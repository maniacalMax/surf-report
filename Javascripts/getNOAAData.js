getData = function()
{
	var location = {};
	var waveHeaders = ["Station", "Latitude", "Longitude", "Swell 1 Height", "Swell 1 Period", "Swell 1 Direction", "Swell 2 Height", "Swell 2 Period", "Swell 2 Direction"];
	var waveHeaderTable = "<tr>";
	for(var i = 0; i < waveHeaders.length; i++)
		{
			waveHeaderTable += "<td><b>" + waveHeaders[i] + "</b></td>";
		}
	waveHeaderTable += "</tr><tr>";
	var windHeaders = ["Station", "Latitude", "Longitude", "Wind Speed", "Wind Direction"];
	var windHeaderTable = "<tr>";
	for(var i = 0; i < windHeaders.length; i++)
		{
			windHeaderTable += "<td><b>" + windHeaders[i] + "</b></td>";
		}
	windHeaderTable += "</tr><tr>";
	location['"latitude (degree)"'] = parseFloat(document.getElementById("latitude").value);
	location['"longitude (degree)"'] = parseFloat(document.getElementById("longitude").value);
	var paramsForTidePredictions = "https://tidesandcurrents.noaa.gov/api/datagetter?range=1&station=8534720&product=water_level&datum=mllw&units=english&time_zone=lst_ldt&format=csv";
    var paramsForBoxWaves = "https://sdf.ndbc.noaa.gov/sos/server.php?request=GetObservation&service=SOS&version=1.0.0&offering=urn:ioos:network:noaa.nws.ndbc:all&featureofinterest=BBOX:-75,37,-72,41&observedproperty=Waves&responseformat=text/csv&eventtime=latest";
    var paramsForBoxWinds = "https://sdf.ndbc.noaa.gov/sos/server.php?request=GetObservation&service=SOS&version=1.0.0&offering=urn:ioos:network:noaa.nws.ndbc:all&featureofinterest=BBOX:-75,38.5,-73.5,41&observedproperty=Winds&responseformat=text/csv&eventtime=latest";
    var paramsForAllWaves = "https://sdf.ndbc.noaa.gov/sos/server.php?request=GetObservation&service=SOS&version=1.0.0&offering=urn:ioos:network:noaa.nws.ndbc:all&observedproperty=Waves&responseformat=text/csv";
	var paramsForAllWinds = "https://sdf.ndbc.noaa.gov/sos/server.php?request=GetObservation&service=SOS&version=1.0.0&offering=urn:ioos:network:noaa.nws.ndbc:all&observedproperty=Winds&responseformat=text/csv";
	
	$.get(paramsForBoxWaves,
			function(waveData, status)
			{
				jsonHTML(csvJSON(waveData), "waveTable", waveHeaderTable);
			});
	$.get(paramsForBoxWinds,
			function(windData, status)
			{
				jsonHTML(csvJSON(windData), "windTable", windHeaderTable);
			});
}

getReport = function()
{
	var location = {};
	location['"latitude (degree)"'] = parseFloat(document.getElementById("latitude").value);
	location['"longitude (degree)"'] = parseFloat(document.getElementById("longitude").value);
	var paramsForTide = "https://tidesandcurrents.noaa.gov/api/datagetter?range=1&station=8534720&product=water_level&datum=mllw&units=english&time_zone=lst_ldt&format=csv";
	var paramsForWaterTemp = "https://tidesandcurrents.noaa.gov/api/datagetter?date=latest&station=8534720&product=water_temperature&units=english&time_zone=lst_ldt&format=csv";
	var paramsForAirTemp = "https://tidesandcurrents.noaa.gov/api/datagetter?date=latest&station=8534720&product=air_temperature&units=english&time_zone=lst_ldt&format=csv";
    var paramsForBoxWaves = "https://sdf.ndbc.noaa.gov/sos/server.php?request=GetObservation&service=SOS&version=1.0.0&offering=urn:ioos:network:noaa.nws.ndbc:all&featureofinterest=BBOX:-75,37,-72,41&observedproperty=Waves&responseformat=text/csv&eventtime=latest";
    var paramsForBoxWinds = "https://sdf.ndbc.noaa.gov/sos/server.php?request=GetObservation&service=SOS&version=1.0.0&offering=urn:ioos:network:noaa.nws.ndbc:all&featureofinterest=BBOX:-75,38.5,-73.5,41&observedproperty=Winds&responseformat=text/csv&eventtime=latest";
    var paramsForAllWaves = "https://sdf.ndbc.noaa.gov/sos/server.php?request=GetObservation&service=SOS&version=1.0.0&offering=urn:ioos:network:noaa.nws.ndbc:all&observedproperty=Waves&responseformat=text/csv";
	var paramsForAllWinds = "https://sdf.ndbc.noaa.gov/sos/server.php?request=GetObservation&service=SOS&version=1.0.0&offering=urn:ioos:network:noaa.nws.ndbc:all&observedproperty=Winds&responseformat=text/csv";
	$.get(paramsForBoxWaves,
		function(waveData, status)
			{
			$.get(paramsForBoxWinds,
			function(windData, status)
				{
				$.get(paramsForTide,
				function(tideData, status)
					{
					$.get(paramsForAirTemp,
					function(airTempData, status)
						{
						$.get(paramsForWaterTemp,
						function(waterTempData, status)
							{
								var wtArray = waterTempData.split(",");
								var waterTemp = wtArray[1];
								var atArray = airTempData.split(",");
								var airTemp = atArray[1];
								buildReportTable(getSwells(findClosestStations(csvJSON(waveData), 2)), findClosestWeatherStation(csvJSON(windData), 1), csvJSON(tideData), waterTemp, airTemp, "reportTable");
							});
						});
					});		
				});
			});
}