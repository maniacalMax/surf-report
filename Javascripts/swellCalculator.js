function convertToCompassDirection(angle)
{
	angle = 450 - angle;
	if(angle < 0)
	{
		angle += 360;
	}
	if(angle > 360)
	{
		angle -= 360;
	}
	return angle;
}

function convertToBackHeading(angle)
{
	if(angle >= 180)
	{
		angle = angle - 180;
	}
	else
	{
		angle = angle + 180;
	}
	return angle;
}

function getSwellAngle(obj, period, direction)
{
	var swellDistance;
	if(parseFloat(obj[period]) < 5)
	{
		swellDistance = 0.25;
	}
	else if(parseFloat(obj[period]) < 7)
	{
		swellDistance = 1;
	}
	else if(parseFloat(obj[period]) < 10)
	{
		swellDistance = 2.5;
	}
	else
	{
		swellDistance = 3;
	}
	
	var deltaLat = swellDistance * Math.cos((Math.PI / 180) * convertToBackHeading(obj[direction]));
	var deltaLng = swellDistance * Math.sin((Math.PI / 180) * convertToBackHeading(obj[direction]));
	var lat = parseFloat(obj['"latitude (degree)"']) + deltaLat;
	var lng = parseFloat(obj['"longitude (degree)"']) + deltaLng;
	var oLat = parseFloat(document.getElementById("latitude").value);
	var oLng = parseFloat(document.getElementById("longitude").value);
	var swellAngle = convertToCompassDirection((180 / Math.PI) * (Math.atan((oLat - lat) / (oLng - lng))));
	return swellAngle;
}

function getSwells(obj)
{
	var threeSwells = [];
	var swells = [];
	for(var i = 0; i < obj.length; i++)
	{
		var swell1 = [];
		if(obj[i]['"sea_surface_wave_significant_height (m)"'] != "")
		{
			swell1[0] = obj[i]['"sea_surface_wave_significant_height (m)"'];
			swell1[1] = obj[i]['"sea_surface_wave_peak_period (s)"'];
			swell1[2] = Math.round(getSwellAngle(obj[i], '"sea_surface_wave_peak_period (s)"', '"sea_surface_wave_to_direction (degree)"'));
			swells.push(swell1);
		}
		var swell2 = [];
		if(obj[i]['"sea_surface_swell_wave_significant_height (m)"'] != "")
		{
			swell2[0] = obj[i]['"sea_surface_swell_wave_significant_height (m)"'];
			swell2[1] = obj[i]['"sea_surface_swell_wave_period (s)"'];
			swell2[2] = Math.round(getSwellAngle(obj[i], '"sea_surface_swell_wave_period (s)"', '"sea_surface_swell_wave_to_direction (degree)"'));
			swells.push(swell2);
		}
	}
	
	var windSwells = [];
	var groundSwells = [];
	for(var i = 0; i < swells.length; i++)
	{
		if(parseFloat(swells[i][1]) < 7)
		{
			windSwells.push(swells[i]);
		}
		else
		{
			groundSwells.push(swells[i]);
		}
	}
	
	for(var i = 0; i < 3; i++)
	{
		threeSwells[i] = [0,0,0];
	}
	
	for(var i = 0; i < windSwells.length; i++)
	{
		if(windSwells[i][0] * windSwells[i][1] > threeSwells[2][0] * threeSwells[2][1])
		{
			threeSwells[2] = windSwells[i]; 
		}
	}
	for(var i = 0; i < groundSwells.length; i++)
	{
		if(groundSwells[i][0] * groundSwells[i][1] > threeSwells[0][0] * threeSwells[0][1])
		{
			threeSwells[1] = threeSwells[0];
			threeSwells[0] = groundSwells[i]; 
		}
		else if(groundSwells[i][0] * groundSwells[i][1] > threeSwells[1][0] * threeSwells[1][1])
		{
			threeSwells[1] = groundSwells[i]; 
		}
	}
	
	return threeSwells;
}