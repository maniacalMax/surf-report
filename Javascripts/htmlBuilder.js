function jsonHTML(obj, htmlTable, header)
{
	var counter = 0;
	for(var i = 0; i < obj.length; i++)
	{
		var lat = parseFloat(obj[i]['"latitude (degree)"']);
		var lng = parseFloat(obj[i]['"longitude (degree)"']);
		var id = obj[i]["station_id"].substring(obj[i]["station_id"].length - 5, obj[i]["station_id"].length).toUpperCase();
		var marker = L.marker([lat, lng]).addTo(mymap);
		var label = "";
		label += "<b>" + id + "</b>";
		marker.bindPopup(label);
		
	}
	var table = "<tr>";
	table += header;
	if(header.includes("Swell"))
	{
		table+= "</tr><tr>";
		for(var i = 0; i < obj.length; i++)
		{
			counter++;
			for(var key in obj[i])
				{
					switch (key)
					{
						case '"sea_surface_wave_significant_height (m)"':
							if(obj[i]['"sea_surface_swell_wave_significant_height (m)"'] != "")
							{
								table += "<td>" + (parseFloat(obj[i]['"sea_surface_swell_wave_significant_height (m)"']) * 3.281).toFixed(2) + " ft</td>";
							}
							else
							{
								table += "<td></td>";
							}
							break;
						case '"sea_surface_wave_peak_period (s)"':
							if(obj[i]['"sea_surface_swell_wave_period (s)"'] != "")
							{
								table += "<td>" + obj[i]['"sea_surface_swell_wave_period (s)"'] + " s</td>";
							}
							else
							{
								table += "<td></td>";
							}
							break;
						case '"sea_surface_swell_wave_significant_height (m)"':
							if(obj[i]['"sea_surface_swell_wave_to_direction (degree)"'] != "")
							{
								table += "<td>" + Math.round(convertToBackHeading(obj[i]['"sea_surface_swell_wave_to_direction (degree)"'])) + "&#176</td>";
							}
							else
							{
								table += "<td></td>";
							}
							break;
						case '"sea_surface_swell_wave_period (s)"':
							if(obj[i]['"sea_surface_wave_significant_height (m)"'] != "")
							{
								table += "<td>" + (parseFloat(obj[i]['"sea_surface_wave_significant_height (m)"']) * 3.281).toFixed(2) + " ft</td>";
							}
							else
							{
								table += "<td></td>";
							}
							break;
						case '"sea_surface_wave_to_direction (degree)"':
							if(obj[i]['"sea_surface_wave_peak_period (s)"'] != "")
							{
								table += "<td>" + obj[i]['"sea_surface_wave_peak_period (s)"'] + " s</td>";
							}
							else
							{
								table += "<td></td>";
							}
							break;
						case '"sea_surface_swell_wave_to_direction (degree)"':
							if(obj[i]['"sea_surface_wave_to_direction (degree)"'] != "")
							{
								table += "<td>" + Math.round(convertToBackHeading(obj[i]['"sea_surface_wave_to_direction (degree)"'])) + "&#176</td>";
							}
							else
							{
								table += "<td></td>";
							}
							break;
						default:
							table += "<td>" + obj[i][key] + "</td>";
					}
				}
			table+= "</tr><tr>";
		}
	}
	else if(header.includes("Wind"))
	{
		for(var i = 0; i < obj.length; i++)
		{
			counter++;
			
			for(var key in obj[i])
				{
					if(key === '"wind_from_direction (degree)"' && obj[i]['"wind_speed (m/s)"'] != 0)
					{
						table += "<td>" + Math.round(obj[i]['"wind_speed (m/s)"'] * 1.94384) + " kts</td>";
					}
					else if(key === '"wind_speed (m/s)"' && obj[i]['"wind_from_direction (degree)"'] != "")
					{
						table += "<td>" + obj[i]['"wind_from_direction (degree)"'] + "&#176</td>";
					}
					else
					{
						table += "<td>" + obj[i][key] + "</td>";
					}
				}
			table+= "</tr><tr>";
		}
	}
	
	document.getElementById(htmlTable).innerHTML = table;
}

function buildReportTable(swells, windData, tideData, waterTemp, airTemp, htmlTable)
{
	
	var lat = document.getElementById("latitude").value;
	var lng = document.getElementById("longitude").value
	var windSpeed = windData[0]['"wind_speed (m/s)"'];
	var tideLevel = tideData[tideData.length -2][" Water Level"];
	windSpeed = Math.round(windSpeed * 1.94384);
	var windDirection = Math.round(windData[0]['"wind_from_direction (degree)"']);
	drawWindTriangle(windSpeed, windDirection);
	lat = lat.substring(0,lat.indexOf(".") + 3);
	lng = lng.substring(0,lng.indexOf(".") + 3);
	for(var i = 0; i < swells.length; i++)
	{
		if(parseFloat(swells[i][0]) > 0)
		{
			drawSwellTriangle(parseFloat(swells[i][0]), parseFloat(swells[i][1]), swells[i][2],i);
		}
	}
	var table = "<tr>";
	table += "<td> <b>Water Temp</b> </td>";
	table += "<td>" + waterTemp + "&#176 F</td>";
	table += "<td> <b>Air Temp</b></td>";
	table += "<td>" + airTemp + "&#176 F</td>";
	table += "</tr><tr>";
	table += "<td> <b>Tide Level</b></td>";
	table += "<td>" + (parseFloat(tideLevel)).toFixed(2) + " ft</td>";
	table += "<td> <b>Tide Direction</b></td>";
	table += (parseFloat(tideData[tideData.length -2][" Water Level"]) > parseFloat(tideData[tideData.length - 3][" Water Level"]))?"<td>Incoming</td>":"<td>Outgoing</td>";
	table += "</tr><tr>";
	table += "<td> <b>Wind Speed</b> </td>";
	table += "<td>" + windSpeed + " kts</td>";
	table += "<td> <b>Wind Direction</b> </td>";
	table += "<td>" + windDirection +  "&#176</td>";
	table += "</tr><tr>";
	table += "<td><b> Swell </b></td>";
	table += "<td><b> Height </b></td>";
	table += "<td><b> Period </b></td>";
	table += "<td><b> Direction </b></td>";
	table += "</tr><tr>";
	table += "<td><font color ='purple'><b> Primary </b></font></td>";
	table += "<td>" + (swells[0][0] * 3.281).toFixed(2) + " ft</td>";
	table += "<td>" + swells[0][1] + " s</td>";
	table += "<td>" + swells[0][2] + "&#176</td>";
	table += "</tr><tr>";
	table += "<td><font color ='blue'><b> Secondary </b></font></td>";
	table += "<td>" + (swells[1][0] * 3.281).toFixed(2) + " ft</td>";
	table += "<td>" + swells[1][1] + " s</td>";
	table += "<td>" + swells[1][2] + "&#176</td>";
	table += "</tr><tr>";
	table += "<td><font color ='brown'><b> Wind </b></font></td>";
	table += "<td>" + (swells[2][0] * 3.281).toFixed(2) + " ft</td>";
	table += "<td>" + swells[2][1] + " s</td>";
	table += "<td>" + swells[2][2] + "&#176</td>";
	table += "</tr>";
	document.getElementById(htmlTable).innerHTML = table;
}

function drawSwellTriangle(height, period, angle, swellNumber)
{
	var oLat = document.getElementById("latitude").value;
	var oLng = document.getElementById("longitude").value;
	var lat = parseFloat(oLat);
	var lng = parseFloat(oLng);
	var deltaLat = Math.cos((Math.PI / 180) * angle)/1000;
	var deltaLng = Math.sin((Math.PI / 180) * angle)/1000;
	var changeInLat = Math.cos((Math.PI / 180) * (angle + 90))/1000;
	var changeInLng = Math.sin((Math.PI / 180) * (angle + 90))/1000;
	var latLngs = [
		[lat + (2*deltaLat),lng + (2*deltaLng)],
		[lat + (4*deltaLat) + (deltaLat * period / 2) + (changeInLat * height * 2),lng + (4*deltaLng) + (deltaLng * period / 2) + (changeInLng * height * 2)],
		[lat + (4*deltaLat) + (deltaLat * period / 2) - (changeInLat * height * 2),lng + (4*deltaLng) + (deltaLng * period / 2) - (changeInLng * height * 2)]
	];
	var popup = L.popup();
	var label = "";
	label += "<b> Height: " + parseFloat(height * 3.281).toFixed(2) + " ft</b>";
	label += "<br><b> Period: " + period + " s</b>";
	label += "<br><b> Angle: " + angle + "&#176</b>";
	var color = "";
	switch(swellNumber)
	{
		case 0:
			color = "purple";
			break;
		
		case 1:
			color = "blue";
			break;
		default:
			color = "brown";
	}
	var polygon = new L.Polygon(latLngs, {color: color, fillOpacity: 1}).addTo(mymap);
	polygon.bindPopup(label);
}

function drawWindTriangle(windSpeed, windDirection)
{
	var oLat = document.getElementById("latitude").value;
	var oLng = document.getElementById("longitude").value;
	var lat = parseFloat(oLat);
	var lng = parseFloat(oLng);
	var deltaLat = Math.cos((Math.PI / 180) * windDirection)/1000;
	var deltaLng = Math.sin((Math.PI / 180) * windDirection)/1000;
	var changeInLat = Math.cos((Math.PI / 180) * (windDirection + 90))/1000;
	var changeInLng = Math.sin((Math.PI / 180) * (windDirection + 90))/1000;
	var latLngs = [
		[lat + (4*deltaLat),lng + (4*deltaLng)],
		[lat + (4*deltaLat) + (windSpeed*deltaLat / 3) + (changeInLat * windSpeed / 3),lng + (4*deltaLng) + (windSpeed*deltaLng / 3) + (changeInLng * windSpeed / 3)],
		[lat + (4*deltaLat) + (windSpeed*deltaLat / 3) - (changeInLat * windSpeed / 3),lng + (4*deltaLng) + (windSpeed*deltaLng / 3) - (changeInLng * windSpeed / 3)]
	];
	var popup = L.popup();
	var label = "";
	label += "<b> Wind Speed: " + windSpeed + " kts</b>";
	label += "<br><b> Wind Direction: " + windDirection + "&#176</b>";
	var color = "";
	if(windSpeed > 20)
	{
		color ="red";
	}
	if(windSpeed > 15)
	{
		color = "orange";
	}
	else if(windSpeed > 10)
	{
		color = "yellow";
	}
	else
	{
		color = "green";
	}
	var polygon = new L.Polygon(latLngs, {color: color, fillOpacity: 1}).addTo(mymap);
	polygon.bindPopup(label);
}