function csvJSON(csv)
{
	  var lines=csv.split("\n");

	  var result = [];

	  var headers=lines[0].split(",");
	  for(var i=1;i<lines.length;i++)
	  {
	      var obj = {};
	      var objString;
	      var currentline=lines[i].split(",");
	      for(var j=0;j<currentline.length;j++)
	      {
	    	  var temp = "";
	    	  if(headers[j] === "station_id")
    		  {
	    		  temp = currentline[j].substring(currentline[j].length - 5, currentline[j].length);
	    		  obj[headers[j]] = temp;
    		  }
	    	  else if(headers[j] === '"latitude (degree)"' || headers[j] === '"longitude (degree)"')
    		  {
	    		  obj[headers[j]] = currentline[j];
    		  }
	    	  else if(headers[j] === '"sea_surface_swell_wave_significant_height (m)"' || headers[j] === '"sea_surface_swell_wave_period (s)"' || headers[j] === '"sea_surface_swell_wave_to_direction (degree)"')
    		  {
	    		  obj[headers[j]] = currentline[j];
    		  }
	    	  else if(headers[j] === '"sea_surface_wave_significant_height (m)"' || headers[j] === '"sea_surface_wave_peak_period (s)"' || headers[j] === '"sea_surface_wave_to_direction (degree)"')
    		  {
	    		  obj[headers[j]] = currentline[j];
    		  }
	    	  else if(headers[j] === '"wind_from_direction (degree)"' || headers[j] === '"wind_speed (m/s)"')
    		  {
	    		  obj[headers[j]] = currentline[j];
    		  }
	    	  else if(headers[j] === '"sea_water_temperature (C)"')
    		  {
	    		  obj[headers[j]] = currentline[j];
    		  }
	    	  else if(headers[j] === '"air_temperature (C)"')
    		  {
	    		  obj[headers[j]] = currentline[j];
    		  }
	    	  else if(headers[j] === " Water Level")
    		  {
	    		  obj[headers[j]] = currentline[j];
    		  }
	    	  else
    		  {
	    		  //obj[headers[j]] = currentline[j];
    		  }
    		  
	      }
	      /*
	      if(obj["station_id"] != "" && obj['"sea_surface_swell_wave_significant_height (m)"'] != "" && obj['"sea_surface_swell_wave_period (s)"'] != "" && obj['"sea_surface_swell_wave_to_direction (degree)"'] != "")
    	  {
    	  	result.push(obj);
    	  }
	      */
	      if(obj["station_id"] != "" && obj["station_id"] != "ocgn4")
    	  {
	    	  if(obj['"sea_surface_swell_wave_significant_height (m)"'] != "" && obj['"sea_surface_swell_wave_period (s)"'] != "" && obj['"sea_surface_swell_wave_to_direction (degree)"'] != "")
	    	  {
	    		  result.push(obj);
	    	  }
	    	  else if(obj['"sea_surface_wave_significant_height (m)"'] != "" && obj['"sea_surface_wave_peak_period (s)"'] != "" && obj['"sea_surface_wave_to_direction (degree)"'] != "")
    		  {
	    		  result.push(obj);
    		  }
    	  }
	      
	  }
	  
	  return result; //JavaScript object
}


function findClosestStations(obj, numberOfStations)
{
	var counter = 0;
	var cutoff = 39.778;
	var objArray = [];
	var multiStationArray = [];
	var distanceArray = [];
	var oLat = parseFloat(document.getElementById("latitude").value);
	var oLng = parseFloat(document.getElementById("longitude").value);
	var minDistance;
	var minIndex;
	if(oLat >= cutoff)
	{
		for(var i = 0; i < obj.length; i++)
		{
			if(parseFloat(obj[i]['"latitude (degree)"']) >= cutoff)
				{
					objArray.push(obj[i]);
				}
		}
	}
	else
	{	
		for(var i = 0; i < obj.length; i++)
		{
			if(parseFloat(obj[i]['"latitude (degree)"']) <= cutoff)
			{
				objArray.push(obj[i]);
			}
		}
	}
	for(var i = 0; i < objArray.length; i++)
	{
		var lat = parseFloat(obj[i]['"latitude (degree)"']);
		var lng = parseFloat(obj[i]['"longitude (degree)"']);
		var distance = Math.sqrt(Math.pow(oLat - lat,2) + Math.pow(oLng - lng, 2));
		distanceArray.push(distance);
	}
	for(k = 0; k < numberOfStations; k++)
	{
		for(var i = 0; i < distanceArray.length;i++)
		{
			if(distanceArray[i] < minDistance || i == 0)
			{
				minDistance = distanceArray[i];
				minIndex = i;
			}
		}
		multiStationArray.push(objArray[minIndex]);
		distanceArray.splice(minIndex, 1);
		objArray.splice(minIndex, 1);
		
	}
	return multiStationArray;

}

function findClosestWeatherStation(obj, numberOfStations)
{
	var counter = 0;
	var multiStationArray = [];
	var distanceArray = [];
	var oLat = parseFloat(document.getElementById("latitude").value);
	var oLng = parseFloat(document.getElementById("longitude").value);
	var minDistance;
	var minIndex;
	for(var i = 0; i < obj.length; i++)
	{
		var lat = parseFloat(obj[i]['"latitude (degree)"']);
		var lng = parseFloat(obj[i]['"longitude (degree)"']);
		var distance = Math.sqrt(Math.pow(oLat - lat,2) + Math.pow(oLng - lng, 2));
		distanceArray.push(distance);
	}
	for(k = 0; k < numberOfStations; k++)
	{
		for(var i = 0; i < distanceArray.length;i++)
		{
			if(obj[i]['"wind_speed (m/s)"'] != "")
			{
				if(distanceArray[i] < minDistance || i == 0)
				{
					minDistance = distanceArray[i];
					minIndex = i;
				}
			}
			
		}
		multiStationArray.push(obj[minIndex]);
		distanceArray.splice(minIndex, 1);
		obj.splice(minIndex, 1);
	}
	
	return multiStationArray;
}